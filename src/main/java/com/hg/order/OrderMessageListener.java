package com.hg.order;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * 顺序消息的监听器
 * created by skh on 2019/12/14
 */
@Component
@Slf4j
public class OrderMessageListener implements MessageListenerOrderly {

	@Override
	public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs, ConsumeOrderlyContext context) {
		try {
			//模拟业务处理消息的时间
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (MessageExt msg : msgs) {
			try {
				log.info("收到消息,消息id:{},消息key:{},消息内容:{}",msg.getMsgId(),msg.getKeys(),new String(msg.getBody(),"UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return ConsumeOrderlyStatus.SUCCESS;
	}
}
