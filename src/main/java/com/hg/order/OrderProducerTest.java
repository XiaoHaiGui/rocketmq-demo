package com.hg.order;

import cn.hutool.json.JSONUtil;
import com.hg.MqConfig;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * created by skh on 2019/12/14
 */
public class OrderProducerTest {

	public static void main(String[] args) throws Exception{
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.hg.order");
		OrderProducer producer =  context.getBean(OrderProducer.class);
		OrderMessageQueueSelector messageQueueSelector = context.getBean(OrderMessageQueueSelector.class);
		String topic = MqConfig.orderTopic;
		String[] statusNames = {"已创建","已付款","已配送","已取消","已完成"};

		//模拟订单消息
		for (int orderId = 1; orderId <= 10; orderId++) {
			//模拟订单的每个状态来发送消息
			for (int i = 0; i < statusNames.length; i++) {
				OrderMessage orderMessage = new OrderMessage();
				orderMessage.setId(orderId);
				orderMessage.setStatus(statusNames[i]);
				orderMessage.setSendOrder(i);
				orderMessage.setContent("hello orderly mq msg");
				String msg = JSONUtil.toJsonStr(orderMessage);
				Message message = new Message(topic,statusNames[i],orderId+"#"+statusNames[i],msg.getBytes());
				//注意:发送消息时,传入参数:队列选择器和订单id,因为要根据订单id来判断消息发送到哪个队列.
				//在OrderMessageQueueSelector 类的 select 方法中的第三个入参能接收到orderId
				SendResult result = producer.getProducer().send(message, messageQueueSelector, orderId);
				System.out.println("result = " + result);
			}
		}

//		context.close();
	}
}
