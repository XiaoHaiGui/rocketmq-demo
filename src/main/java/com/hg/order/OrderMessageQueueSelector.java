package com.hg.order;

import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 顺序消息的队列选择器,为了把保持顺序的消息
 * created by skh on 2019/12/14
 */
@Component
public class OrderMessageQueueSelector implements MessageQueueSelector {
	/*
	本例是将同一个订单（ 即具有相同的 orderld ）的消息按状态先后顺序消费的， 所以消息生
	产者调用 send 方法发送时需要传入 MessageQueueSelector 接口的实现类，将 orderId 相同 的消
	息放入同一个 MessageQueue 中 。 为简单起见 ， 这里的算法是根据 orderId 取余的，在实际场景
	中可 以根据需要自定义。
	 */
	@Override
	public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
		Integer orderId = (Integer) arg; //该参数由生产者调用send方法时传入
		return mqs.get(orderId % mqs.size());
	}
}
