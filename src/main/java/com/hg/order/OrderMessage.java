package com.hg.order;

import lombok.Data;

/**
 * created by skh on 2019/12/14
 */
@Data
public class OrderMessage {

	private Integer id; //订单id

	private String status; //订单状态

	private int sendOrder; //订单消息发送顺序

	private String content; //订单内容
}
