package com.hg.order;

import com.hg.MqConfig;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * created by skh on 2019/12/14
 */

@Data
@Slf4j
//@Component
public class OrderProducer {

	private DefaultMQProducer producer;

	@PostConstruct
	public void init() throws Exception {
		log.info("开始启动生产者服务");
		producer = new DefaultMQProducer(MqConfig.producerGroup);
		producer.setNamesrvAddr(MqConfig.nameServerAddr);
		producer.start();
		log.info("生产者服务启动成功");
	}

	@PreDestroy
	public void destroy() throws Exception {
		log.info("开始关闭生产者服务");
		producer.shutdown();
		log.info("生产者服务关闭成功");
	}
}
