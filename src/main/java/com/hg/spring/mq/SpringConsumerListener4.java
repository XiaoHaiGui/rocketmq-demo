package com.hg.spring.mq;

import com.hg.order.OrderMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @Author: skh
 * @Date: 2019/12/16 14:50
 * @Description:
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "test-topic-3",consumerGroup = "consumer4-group",consumeMode = ConsumeMode.ORDERLY)
public class SpringConsumerListener4 implements RocketMQListener<OrderMessage> {


    @Override
    public void onMessage(OrderMessage message) {
        log.info("监听者4收到消息:{}", message);
    }
}
