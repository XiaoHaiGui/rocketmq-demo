package com.hg.spring.mq;

import cn.hutool.json.JSONUtil;
import com.hg.order.OrderMessage;
import com.hg.transaction.entity.User;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.common.message.MessageConst;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @Author: skh
 * @Date: 2019/12/16 15:12
 * @Description:
 */
@Component
public class SpringProducer {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    public void sendNormalMsg() {
        rocketMQTemplate.convertAndSend("test-topic-1", "Hello, World!");
    }

    public void sendMsg() {
        rocketMQTemplate.convertAndSend("test-topic-1", "Hello, World!");
        rocketMQTemplate.send("test-topic-1", MessageBuilder.withPayload("Hello, World! I'm from spring message").build());
        User user = new User();
        user.setId(1);
        user.setUserName("skh");
        rocketMQTemplate.convertAndSend("test-topic-2",user );
    }

    public void sendDelayMsg() {
        // 发送延迟消息
        Message<String> message = MessageBuilder.withPayload("I'm delayed message").setHeader(MessageConst.PROPERTY_KEYS, 3124).build();
        //30秒后发送
        rocketMQTemplate.asyncSend("test-topic-1", message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("发送成功");
            }

            @Override
            public void onException(Throwable throwable) {

            }
        },10000, 4);

    }

    public void sendOrderlyMsg() {
        String[] statusNames = {"已创建","已付款","已配送","已取消","已完成"};

        //模拟订单消息
        for (int orderId = 1; orderId <= 10; orderId++) {
            //模拟订单的每个状态来发送消息
            for (int i = 0; i < statusNames.length; i++) {
                OrderMessage orderMessage = new OrderMessage();
                orderMessage.setId(orderId);
                orderMessage.setStatus(statusNames[i]);
                orderMessage.setSendOrder(i);
                orderMessage.setContent("hello orderly mq msg");
                SendResult sendResult = rocketMQTemplate.syncSendOrderly("test-topic-3", orderMessage, String.valueOf(orderId));
                System.out.println("sendResult = " + sendResult);
            }
        }
    }

    public void sendTransactionMsg() {
        Message<String> message = MessageBuilder.withPayload("local-tx-success").build();
        TransactionSendResult result = rocketMQTemplate.sendMessageInTransaction("tx-producer-group", "transcation-topic", message, "success");
        System.out.println("result = " + result);
        Message<String> message1 = MessageBuilder.withPayload("local-tx-fail").build();
        TransactionSendResult result2 = rocketMQTemplate.sendMessageInTransaction("tx-producer-group", "transcation-topic", message1, "fail");
        System.out.println("result2 = " + result2);
    }
}
