package com.hg.spring.mq;

import com.hg.transaction.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @Author: skh
 * @Date: 2019/12/16 14:50
 * @Description:
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "test-topic-2",consumerGroup = "consumer2-group")
public class SpringConsumerListener2 implements RocketMQListener<MessageExt> {


    @Override
    public void onMessage(MessageExt message) {
        log.info("收到消息:{}", message);
    }
}
