package com.hg.spring.mq;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @Author: skh
 * @Date: 2019/12/16 14:50
 * @Description:
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "test-topic-1",consumerGroup = "consumer-group")
public class SpringConsumerListener implements RocketMQListener<MessageExt> {



    @Override
    public void onMessage(MessageExt message) {
        int reconsumeTimes = message.getReconsumeTimes();
        log.info("收到消息,重试次数:{}", reconsumeTimes);
        if (reconsumeTimes == 3) {
            log.info("重试次数达到3次");
        }else {
            throw new RuntimeException("异常测试");
        }
    }
}
