package com.hg.spring.mq;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @Author: skh
 * @Date: 2019/12/16 14:50
 * @Description:
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "transcation-topic",consumerGroup = "consumer6-group")
public class SpringConsumerListener6 implements RocketMQListener<String> {

    @Override
    public void onMessage(String message) {
        log.info("收到消息:{}", message);
    }
}
