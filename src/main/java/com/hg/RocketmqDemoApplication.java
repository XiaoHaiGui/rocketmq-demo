package com.hg;

import com.hg.spring.mq.SpringProducer;
import com.hg.transaction.entity.User;
import com.hg.transaction.service.UserService;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.support.MessageBuilder;

import java.math.BigDecimal;

@SpringBootApplication
/*
@MapperScan("com.demo.mapper")：
扫描指定包中的接口

@MapperScan("com.demo.*.mapper")：
一个*代表任意字符串，但只代表一级包,比如可以扫到com.demo.aaa.mapper,不能扫到com.demo.aaa.bbb.mapper

@MapperScan("com.demo.**.mapper")：
两个*代表任意个包,比如可以扫到com.demo.aaa.mapper,也可以扫到com.demo.aaa.bbb.mapper
 */
@MapperScan(basePackages = "com.hg.**.dao")
public class RocketmqDemoApplication implements CommandLineRunner {

	@Autowired
	SpringProducer springProducer;

	@Autowired
	private UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(RocketmqDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		springProducer.sendNormalMsg();

	}
}
