package com.hg;

import lombok.Data;

/**
 * created by skh on 2019/12/14
 */
public interface MqConfig {

	String nameServerAddr = "47.98.129.125:9876";

	String producerGroup = "hg_producer_group";

	String consumerGroup = "hg_consumer_group";

	String orderTopic = "topic_example_order";


}
