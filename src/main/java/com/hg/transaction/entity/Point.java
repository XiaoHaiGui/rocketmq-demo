package com.hg.transaction.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * created by skh on 2019/12/15
 */
@Data
@TableName("tb_point")
public class Point {

	@TableId
	private Integer id;

	private Integer userId;

	private Integer amount;
}
