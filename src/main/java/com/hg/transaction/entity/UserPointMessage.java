package com.hg.transaction.entity;

import lombok.Data;

/**
 * created by skh on 2019/12/15
 */
@Data
public class UserPointMessage {

	private Integer userId;

	private String userName;

	private Integer amount;
}
