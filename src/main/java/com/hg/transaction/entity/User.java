package com.hg.transaction.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.annotation.sql.DataSourceDefinition;

/**
 * created by skh on 2019/12/15
 */
@Data
@TableName("tb_user")
public class User {

	@TableId
	private Integer id;

	private String userName;
}
