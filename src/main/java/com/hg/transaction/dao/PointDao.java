package com.hg.transaction.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hg.transaction.entity.Point;

/**
 * @Author: skh
 * @Date: 2019/12/16 11:03
 * @Description:
 */
public interface PointDao extends BaseMapper<Point> {
}
