package com.hg.transaction.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hg.transaction.entity.User;

/**
 * created by skh on 2019/12/15
 */

public interface UserDao extends BaseMapper<User> {

}
