package com.hg.transaction.listener;

import com.hg.transaction.dao.PointDao;
import com.hg.transaction.entity.Point;
import com.hg.transaction.entity.UserPointMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: skh
 * @Date: 2019/12/16 10:55
 * @Description:
 */
@Component
@Slf4j
@RocketMQMessageListener(topic = "user", consumerGroup = "consumer7-group")
public class TransactionMessageListener implements RocketMQListener<UserPointMessage> {

    @Autowired
    private PointDao pointDao;


    @Override
    public void onMessage(UserPointMessage msg) {

        log.info("接收到消息:{}", msg);
        Point point = new Point();
        point.setUserId(msg.getUserId());
        point.setAmount(msg.getAmount());
        //throw new RuntimeException("point新增异常");
        pointDao.insert(point);
        log.info("新增积分成功");
    }
}
