package com.hg.transaction.listener;

import ch.qos.logback.core.encoder.ByteArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.hg.transaction.entity.User;
import com.hg.transaction.entity.UserPointMessage;
import com.hg.transaction.service.UserService;
import com.sun.org.apache.xpath.internal.operations.String;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @Author: skh
 * @Date: 2019/12/16 10:42
 * @Description:
 */
@Component
@Slf4j
@RocketMQTransactionListener
public class UserLocalTransactionListener implements RocketMQLocalTransactionListener {

    @Autowired
    private UserService userService;


    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message msg, Object arg) {
        try {
            log.info("执行本地事务");
            log.info("收到消息内容:{},arg:{}",msg,arg);

            //userService.addUser(toBean.getUserId(), toBean.getUserName());
        } catch (Exception e) {
            log.error("本地事务执行异常",e);
            return RocketMQLocalTransactionState.ROLLBACK;
        }

        return RocketMQLocalTransactionState.COMMIT;
    }

    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message msg) {
        log.info("回查本地事务");
        log.info("收到消息内容:{}",msg);
        Object payload1 = msg.getPayload();

        return RocketMQLocalTransactionState.COMMIT;
    }
}
